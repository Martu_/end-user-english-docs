# Tutorials & Documentation

Full documentation on the use of MaadiX.

[Web oficial de MaadiX](https://maadix.net/en)

!!! Warning

    You are reading the documentation for Control Panel Version 2.0.
    For Version 1.0 see this page: [Documentation for Version 1.0](https://docs.maadix.net/)


![Control Panel](img/en/panel-de-control.png)

# What is MaadiX?

MaadiX is a tool that allows you to enable open source applications on your own server with one click. In addition, you can manage them through a graphical interface without needing **technical expertise** or **major investments**. In this guide you will find instructions on how to use the graphical administration interface.

# How does it work?

MaadiX integrates a series of standardized system configurations, allowing you to manage your own virtual servers and applications from an easy and intuitive graphical interface. It allows you to install advanced applications such as OpenVPN, Etherpad, mail server, Mailman, Let's Encrypt or Owncloud among others, all in one simple click. With MaadiX, you can have your own independent, private and secure cloud.

# Control Panel

The control panel is the interface that allows you to perform various administration and maintenance tasks without having to execute commands on the terminal. You can install and manage applications; create user accounts, domains and email accounts; grant access to the system to other people with different types of privileges and view basic system statistics, among other operations.

# Data

All servers have installed a directory where the data of users, domains, e-mail accounts and applications will be stored (technically speaking, this is an LDAP directory).
The control panel is the graphical interface for managing this directory, and the system will pull the required information from it for configuration. This way, the control panel does not need the privileges needed to write this information directly to the system.

# Changes since Version 1.0

If you have updated the version of MaadiX from a previous installation, you will have noticed some changes not only in its appearance (graphic interface), but also in some of its features:

* In this new version, the possibility of visiting the control panel from any domain created on the server has been removed. It will only be available through the main domain of the server (FQDN).
* The minimum number of characters required for passwords has been increased to 10.
