# Email

You can add an infinite number of email accounts for all the domains you have activated on your server.
Before adding email accounts, it is important that you make sure you have correctly configured the corresponding domain. To find out, check out [Domain configuration](/domains).

# Create an account

In the section **'Mail'** -> **'Mail Accounts'** , click on the button 'Add a new account' and fill in the different fields of the form that is displayed. In the field 'User' you will have to insert only the name of the account (without '@domain.com') and you will have to choose a domain from the list of available domains, which you will have previously activated in the section 'Domains'.
To create the account 'user3@example.com' you will have to insert user3 in the field 'User' and choose the domain 'example.com' from the drop-down menu of the field 'Domain'.

![Screenshot](img/en/add-mail.png)

# Editing an account

You can change the parameters of the email accounts you have activated by visiting the section **'Mail'** -> **'Mail Accounts'** and then clicking on the button **'Edit account'** corresponding to the account you want to modify.

![Edit email](img/en/mails/edit-email-link.png)

# Automatic Forward and Automatic Response (Vacation)

On the same editing page of each email account you can enable automatic forwarding of all incoming mail to any other email address.

To do this, you have to check the box **'Enable forward'** and insert a valid address in the new field that is displayed. In case you want it to forward to multiple accounts, you must separate each one with a comma (user1@example.com,user2@example.com). If you want to continue receiving a copy of incoming mail to your current account, you will need to include it in the list.

To activate the automatic response (out of office) check the box **'Enable automatic response'**. In the field below **'Automatic reply message'** insert the text you want to send.

![Forward email](img/es/mails/edit-mail.png)

# Delete an account

When you delete a mail account, the messages in its folders (received, sent, trash...) are not deleted and a copy is saved on your server. The account is deactivated, so you can no longer send or receive mail. If you create the same account again in the future, you will recover all the contents of your folders.
To permanently empty a mail account, you can delete its contents using the webmail interface (if you have the application installed) or using an e-mail client (Thunderbird, Outlook...) configured with IMAP. IMAP creates a synchronization between the client and the server, so that all the actions performed in one of the two parts are reflected in the other.

# Webmail - Rainloop

If you have installed the Rainloop application on your server, you can use this webmail tool to check and send email from your browser.
From the control panel itself you have direct access to the webmail application (Rainloop). You can use the url yoursubdomain.maadix.org/rainloop, and also any domain you have configured on your server. For example, if you have successfully configured the domain example.com, then you can visit the webmail interface by visiting https://example.com/rainloop

All email accounts successfully activated through the control panel can be accessed through the webmail interface, if Rainloop has been installed.

# Email Client

MaadiX allows you to check your email using an email client you have installed on your computer (Thunderbird, Outlook...)
In order to be able to configure your account for the client, you need the server connection details, which can be found on the same page **'Edit account'** by clicking on the IMAP or POP3 button at the top right.
